<?php

return [
    Symfony\Bundle\FrameworkBundle\FrameworkBundle::class => ['all' => true],
    Enqueue\Bundle\EnqueueBundle::class => ['all' => true],
    Abc\JobWorkerBundle\AbcJobWorkerBundle::class => ['all' => true],
];
