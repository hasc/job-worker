# abc-job-worker

[![pipeline status](https://gitlab.com/hasc/job-worker/badges/master/pipeline.svg)](https://gitlab.com/hasc/job-worker/commits/master)

A Symfony application providing the job worker from [AbcJobWorkerBundle](https://github.com/aboutcoders/job-worker-bundle) using RabbitMQ as transport layer.

**Note: This project is still in an experimental phase!**

## Demo

You can find a demo [here](https://gitlab.com/hasc/abc-job-demo/).

## Environment Variables

### `APP_ENV`

This variable is optional, it specifies the application runtime environment. Supported values are `prod` and `dev` (default is `prod`).

### `APP_DEBUG`

This variable is optional, it specifies whether the application is executed in debug mode (default is false).

### `ENQUEUE_DSN`

This variable is mandatory, it specifies the DSN of the enqueue transport, see: <https://github.com/php-enqueue/enqueue-dev/blob/master/docs/bundle/config_reference.md>

### `JOB_SERVER_URL`

This variable is mandatory, it specifies the URL of the job server.

## Building the application

```bash
docker build -t job-worker:local .
```

Development images:

```bash
docker build -t job-worker:local-dev --build-arg APP_ENV=dev .
```

Dockerfile is created based on <https://sagikazarmark.hu/blog/containerizing-a-symfony-application/>

## Application Details

The application is based on a standard Symfony application 4.3.x with the following bundles installed:

* [EnqueueBundle](https://github.com/php-enqueue/enqueue-bundle)
* [AbcWorkerBundle](https://github.com/aboutcoders/job-worker-bundle)

### Routes

A route must be configured for every job. A route defines the name of the queue a job is sent to and the name of the queue where replies of the job are sent to. Before the job server can start accepting jobs some routes must be registered.

By tagging a class with `abc.job.route_provider` the configured routes will be registered at the job server when the command `abc:process:job` is run. Don't use this tag if you want to configure the routes yourself (see API documentation).

The route configuration is defined in the class [App\JobRoutes](./src/JobRoutes.php) and tagged with `abc.job.route_provider` in [service.yaml](./config/services.yaml).

```yaml
app.job_routes:
    class: App\JobRoutes
    tags:
        - { name: 'abc.job.route_provider' }
```


### Job Registration

The jobs are registered in [services.yaml](./config/services.yaml) like this:

```yaml
app.jobA:
    class: Abc\Job\Processor\TestProcessor
    public: true
    tags:
        - { name: 'abc.job.processor', jobName: 'job_A'}

app.jobB:
    class: Abc\Job\Processor\TestProcessor
    public: true
    tags:
        - { name: 'abc.job.processor', jobName: 'job_B'}

app.jobC:
    class: Abc\Job\Processor\TestProcessor
    public: true
    tags:
        - { name: 'abc.job.processor', jobName: 'job_C'}
```



## License

The MIT License (MIT). Please see [License File](./LICENSE) for more information.
