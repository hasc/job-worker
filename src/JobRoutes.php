<?php

namespace App;

use Abc\Job\RouteProviderInterface;

class JobRoutes implements RouteProviderInterface
{
    public static function getRoutes()
    {
        return [
            [
                'name' => 'job_A',
                'queue' => 'abc.default',
                'replyTo' => 'abc.reply',
            ],
            [
                'name' => 'job_B',
                'queue' => 'abc.default',
                'replyTo' => 'abc.reply',
            ],
            [
                'name' => 'job_C',
                'queue' => 'abc.default',
                'replyTo' => 'abc.reply',
            ],
        ];
    }
}
